//
//  ViewController.swift
//  nfcTag
//
//  Created by Feker Hassine on 2021-10-31.
//

import UIKit
import CoreNFC
import MobileCoreServices
import CoreImage.CIFilterBuiltins

let MAX_TRANSFER_SIZE: Int = 246;
let passwordSize = 13
let BITMAP_SIZE = 4798

enum nfcMethod{
    case read
    case write
}

class ViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    var session: NFCTagReaderSession?
    var method: nfcMethod = .read
    var imageData: Data = Data()
    var roomData: Data = Data()
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }


    @IBAction func nfcReaderButtonTapped(_ sender: Any) {
        method = .read
        startNfcModule()
    }
    
    @IBAction func nfcWriterButtonTapped(_ sender: Any) {
        displayPasswordVerificationAlert()
    }
    
    private func startNfcModule(){
        session = NFCTagReaderSession(pollingOption: [.iso14443], delegate: self, queue: DispatchQueue.main)
        session?.alertMessage = "Hold your tag near an NFC tag"
        session?.begin()
    }
    
    private func getImageData() -> Data
    {
        if let image = containerView.image()?.monochrome?.rotate(radians: .pi/2) {
            var path = documentDirectoryPath()?.appendingPathComponent("exampleBmp.png")
            try? image.pngData()?.write(to: path!)
            
            let bytesHex = "424DBE120000000000003E00000028000000800000002801000001000100000000008812000000000000000000000200000002000000000000FFFFFFFFFF\(bitsToBytes(bits: image.pixelData).bytesToHex())"
            print(bytesHex)
            return bytesHex.hexadecimal!
        }
        return Data()
    }
    
    private func bitsToBytes(bits: [Int]) -> [UInt8] {
         let numBytes = 1 + (bits.count - 1) / 8
         var bytes = [UInt8](repeating: 0, count: numBytes)

         for (index, bit) in bits.enumerated() {
             if bit == 1 {
                 bytes[index / 8] += UInt8(1 << (7 - index % 8))
             }
         }
         return bytes
    }
    
    private func documentDirectoryPath() -> URL? {
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return path.first
    }
    
    func displayPasswordVerificationAlert(){
        let alertController = UIAlertController(title: "Nom", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Entrer votre nom"
        }
        
        let saveAction = UIAlertAction(title: "Write data", style: UIAlertAction.Style.default, handler: {alert -> Void in
            DispatchQueue.main.async {[weak self] in
                guard let self = self else { return }
                
                self.nameLabel.text = alertController.textFields?[0].text
                let imageData = self.getImageData()
                self.imageData = imageData
                
                let roomObject = Room(TemplateId: Template(Id: 2), Field1: alertController.textFields?[0].text, Field2: "", Field3: "", Field4: "", Field5: "", Number: "", Floor: "")
                
                let jsonEncoder = JSONEncoder()
                let jsonData = try? jsonEncoder.encode(roomObject)
                self.roomData = jsonData!
                                
                self.method = .write
                self.startNfcModule()
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
}

extension ViewController: NFCTagReaderSessionDelegate{    
    func tagReaderSessionDidBecomeActive(_ session: NFCTagReaderSession) {
        print("session has became active")
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didInvalidateWithError error: Error) {
        print(error)
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didDetect tags: [NFCTag]) {
        if tags.count > 1{
            session.alertMessage = "more than one tag has been detected"
            session.restartPolling()
            return
        }
        
        let singleTag = tags.first!
        
        if case let NFCTag.iso7816(tag) = tags.first! {
            print("tag")
            session.connect(to: singleTag) { error in 
                if let _ = error as? NFCReaderError{
                    session.alertMessage = "unable to connect to tag"
                    session.invalidate()
                    return
                }
                
                let nfc = nfcManipulation()
                nfc.method = self.method
                nfc.delegate = self
                nfc.imageData = self.imageData
                nfc.datas = self.roomData
                nfc.selectNdefTagApplication(tag: tag, session: session)

            }
        }
    }
}

//MARK: - UIView extension: capture image from UIView
extension UIView {

    /// Creates an image from the view's contents, using its layer.
    ///
    /// - Returns: An image, or nil if an image couldn't be created.
    func image() -> UIImage? {
        UIGraphicsBeginImageContext(self.frame.size)
        self.layer.render(in:UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return UIImage(cgImage: image!.cgImage!)
    }
}

//MARK: - UIImage extension: Image rotation
extension UIImage {
    
    func rotate(radians: CGFloat) -> UIImage? {
        let rotatedSize = CGRect(origin: .zero, size: size)
            .applying(CGAffineTransform(rotationAngle: CGFloat(radians)))
            .integral.size
        UIGraphicsBeginImageContext(rotatedSize)
        if let context = UIGraphicsGetCurrentContext() {
            let origin = CGPoint(x: rotatedSize.width / 2.0,
                                 y: rotatedSize.height / 2.0)
            context.translateBy(x: origin.x, y: origin.y)
            context.rotate(by: radians)
            draw(in: CGRect(x: -origin.y, y: -origin.x,
                            width: size.width, height: size.height))
            let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return rotatedImage ?? self
        }
        
        return self
    }
    
    var pixelData: [Int] {
        let bmp = self.cgImage!.dataProvider!.data
        var data: UnsafePointer<UInt8> = CFDataGetBytePtr(bmp)
        var r, g, b, a: UInt8
        var bits = [Int]()

        for row in 0 ..< Int(self.size.width) {
            for col in 0 ..< Int(self.size.height) {
                r = data.pointee
                data = data.advanced(by: 1)
                g = data.pointee
                data = data.advanced(by: 1)
                b = data.pointee
                data = data.advanced(by: 1)
                a = data.pointee
                data = data.advanced(by: 1)
                let pixel = Pixel(r: r, g: g, b: b, a: a, row: row, col: col)
                var white: CGFloat = 0
                var alpha: CGFloat = 0
                pixel.color.getWhite(&white, alpha: &alpha)
                if white > 0.5 {
                    bits.append(1)
                } else {
                    bits.append(0)
                }
            }
        }
        return bits
    }

    var monochrome: UIImage? {
        let context = CIContext(options: nil)
        let currentFilter = CIFilter(name: "CIColorMonochrome")
        currentFilter?.setValue(CIImage(image: self), forKey: kCIInputImageKey)
        guard let output = currentFilter?.outputImage,
              let cgImage = context.createCGImage(output, from: output.extent) else {
            print("Failed to create output image")
            return nil
        }
        return UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)
    }
    
    func toData (options: NSDictionary, type: ImageType) -> Data? {
        guard cgImage != nil else { return nil }
        return toData(options: options, type: type.value)
    }

    // about properties: https://developer.apple.com/documentation/imageio/1464962-cgimagedestinationaddimage
    func toData (options: NSDictionary, type: CFString) -> Data? {
        guard let cgImage = cgImage else { return nil }
        return autoreleasepool { () -> Data? in
            let data = NSMutableData()
            guard let imageDestination = CGImageDestinationCreateWithData(data as CFMutableData, type, 1, nil) else { return nil }
            CGImageDestinationAddImage(imageDestination, cgImage, options)
            CGImageDestinationFinalize(imageDestination)
            return data as Data
        }
    }

    // https://developer.apple.com/documentation/mobilecoreservices/uttype/uti_image_content_types
    enum ImageType {
        case bmp                        // Windows bitmap

        var value: CFString {
            switch self {
            case .bmp: return kUTTypeBMP
            }
        }
    }
}

struct Pixel {

    var r: Float
    var g: Float
    var b: Float
    var a: Float
    var row: Int
    var col: Int

    init(r: UInt8, g: UInt8, b: UInt8, a: UInt8, row: Int, col: Int) {
        self.r = Float(r)
        self.g = Float(g)
        self.b = Float(b)
        self.a = Float(a)
        self.row = row
        self.col = col
    }

    var color: UIColor {
        return UIColor(
            red: CGFloat(r/255.0),
            green: CGFloat(g/255.0),
            blue: CGFloat(b/255.0),
            alpha: CGFloat(a/255.0)
        )
    }

    var description: String {
        return "\(r), \(g), \(b), \(a)"
    }

}

extension Array where Element == UInt8 {
    
    func bytesToHex() -> String {
        var hexString: String = ""
        for byte in self
        {
            hexString.append(String(format:"%02X", byte))
        }
        return hexString
    }
}
