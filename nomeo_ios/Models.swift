//
//  Models.swift
//  nfcTag
//
//  Created by Feker Hassine on 2021-11-12.
//

import Foundation

struct Room: Codable {
    let TemplateId: Template
    let Field1, Field2, Field3, Field4: String?
    let Field5, Number, Floor: String?
    
    private enum CodingKeys: String, CodingKey {
        case TemplateId, Field1, Field2, Field3, Field4, Field5, Number, Floor
    }


}

// MARK: - Template
struct Template: Codable {
    let Id: Int?
}
