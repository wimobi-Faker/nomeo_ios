//
//  nfcManipulation.swift
//  nfcTag
//
//  Created by Feker Hassine on 2021-11-12.
//

import CoreNFC
import UIKit

class nfcManipulation {
    var method: nfcMethod = .read
    weak var delegate: ViewController?
    var imageData: Data = Data()
    var datas: Data = Data()
    
    func selectNdefTagApplication(tag: NFCISO7816Tag, session: NFCReaderSession, sendInterrupt: Bool = false) {
        let dataMifare: [UInt8] = [0xD2, 0x76, 0x00, 0x00, 0x85, 0x01, 0x01]
        let dataPacketMifare = Data(bytes: dataMifare, count: dataMifare.count)
        
        session.alertMessage = "Selecting tag application"
        
        
        let myAPDU = NFCISO7816APDU(instructionClass: 0x00, instructionCode: 0xA4, p1Parameter: 0x04, p2Parameter: 0x00, data: dataPacketMifare, expectedResponseLength: 0x01)
        tag.sendCommand(apdu: myAPDU) {(response: Data, sw1: UInt8, sw2: UInt8, error: Error?) in
            guard error == nil && (sw1 == 0x90 && sw2 == 0) else {
                print(error?.localizedDescription ?? "")
                session.invalidate(errorMessage: "Applicationfailure")
                return
            }
            print(response)
            !sendInterrupt ? self.NdefSelectFileCmd(tag: tag, session: session) : self.selectSystemFile(tag: tag, session: session)
        }
    }
        
    func NdefSelectFileCmd(tag: NFCISO7816Tag, session: NFCReaderSession){
        let dataMifare: [UInt8] = [0x00, 0x01]
        let dataPacketMifare = Data(bytes: dataMifare, count: 2)
        
        
        let myAPDU = NFCISO7816APDU(instructionClass: 0x00, instructionCode: 0xA4, p1Parameter: 0x00, p2Parameter: 0x0C, data: dataPacketMifare, expectedResponseLength: -1)
        
        session.alertMessage = "Selecting NDEF file"
        
        tag.sendCommand(apdu: myAPDU) {(response: Data, sw1: UInt8, sw2: UInt8, error: Error?) in
            guard error == nil && (sw1 == 0x90 && sw2 == 0) else {
                print(error?.localizedDescription ?? "")
                session.invalidate(errorMessage: "Applicationfailure")
                return
            }
            print(response)
            
            let response: Data? = nil
            
            switch self.method
            {
                case .read:
                    self.readBinary(tag: tag, session: session, p1: 0, p2: 0, length: 2) {data in
                        
                        let fileSize = (self.byteToUnsignedInt(byteToConvert: data[0]) << 8) + self.byteToUnsignedInt(byteToConvert: data[1])
                        
                        self.readData(tag: tag, session: session, fileOffset: BITMAP_SIZE + passwordSize, sizeInBytes: fileSize, currentResponse: response)
                    }
                    break
                
                case .write:
                    self.verifyPasswordForWriteAccess(tag: tag, session: session) { _ in
                        
                        self.disablePasswordVerificationRequirement(tag: tag, session: session) { _ in
                            print("verification completed")
                            
                            self.readBinary(tag: tag, session: session, p1: 0, p2: 0, length: 2) { fileLengthData in
                                let fileSize = (self.byteToUnsignedInt(byteToConvert: fileLengthData[0]) << 8) + self.byteToUnsignedInt(byteToConvert: fileLengthData[1])
                                
                                var currentData = Data(repeating: UInt8(), count: fileSize)
                                currentData[0...self.datas.count-1] = self.datas[0...self.datas.count-1]
                                
                                var passwordData: Data = Data()
                                passwordData.append(contentsOf: [172, 237, 0, 5, 116, 0, 6, 0, 0, 0, 0, 0, 0])
                                
                                
                                self.writeData(tag: tag, session: session, fileOffset: 0, image: self.imageData, password: passwordData, datas: currentData, remainingDataToWrite: self.imageData.count + currentData.count + passwordData.count)
                            }
                        
                            }
                            }
                            

                    break
                
            }
        }
    }
    
    
    func byteToUnsignedInt(byteToConvert: UInt8) -> Int {
        return Int(byteToConvert & 255);
    }
    
    
    func readData(tag: NFCISO7816Tag, session: NFCReaderSession, fileOffset: Int, sizeInBytes: Int, currentResponse: Data?)
    {
        var response: Data? = currentResponse
        var currentFileOffset: Int = fileOffset;
        var dataChunktoRead: Int;
        var remainingDataToRead: Int = sizeInBytes;
        if (remainingDataToRead > 0)
        {
            dataChunktoRead = remainingDataToRead > MAX_TRANSFER_SIZE ? MAX_TRANSFER_SIZE : remainingDataToRead
            
            let p1: UInt8 = UInt8((currentFileOffset & 65280) >> 8 )
            let p2: UInt8 = UInt8((currentFileOffset & 255));
            let length: UInt8 = UInt8((dataChunktoRead & 255));

            readBinary(tag: tag, session: session, p1: p1, p2: p2, length: length) { data in
                print("here")
                if response == nil{
                    response = Data(count: sizeInBytes)
                }
                
                if data.count != 0{
                    for n in 0...data.count-1{
                        response![currentFileOffset - (BITMAP_SIZE + passwordSize) + n] = data[n]
                    }
                }

                currentFileOffset += dataChunktoRead
                remainingDataToRead -= dataChunktoRead
                
                self.readData(tag: tag, session: session, fileOffset: currentFileOffset, sizeInBytes: remainingDataToRead, currentResponse: response)
            }
        }else{
            response = trim(bytes: response!)
            
            session.alertMessage = "Reading data process completed"
            
            session.invalidate()
            
            let room = try? JSONDecoder().decode(Room.self, from: response!)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            guard let dataDisplayVC = storyboard.instantiateViewController(withIdentifier: "DataDisplayController") as? DataDisplayController else { return }
            dataDisplayVC.room = room
            
            self.delegate!.navigationController?.pushViewController(dataDisplayVC, animated: true)
                                    
        }
    }
    
    func trim(bytes: Data) -> Data
    {
        var i: Int = bytes.count - 1;
        while (i >= 0 && (bytes[i] == 0))
        {
            i-=1
        }

        return bytes[0...i]
    }
    
    func writeData(tag: NFCISO7816Tag, session: NFCReaderSession, fileOffset: Int, image: Data, password: Data, datas: Data, remainingDataToWrite: Int){
        var currentFileOffset: Int = fileOffset
        var dataChunkToWrite: Int = 0
        var currentOffsetInSrcDataFile: Int = fileOffset
        
        var currentRemainingDataToWrite = remainingDataToWrite
        
        var allDatas: Data = Data(count: image.count + password.count + datas.count)
        allDatas[0...image.count-1] = image[0...image.count-1]
        allDatas[image.count...image.count+password.count-1] = password[0...password.count-1]
        allDatas[image.count+password.count...allDatas.count-1] = datas[0...datas.count-1]
                        
        if remainingDataToWrite > 0{
            dataChunkToWrite = currentRemainingDataToWrite > MAX_TRANSFER_SIZE ? MAX_TRANSFER_SIZE : currentRemainingDataToWrite
            var buffer: Data = Data(count: dataChunkToWrite)
            
            for n in currentOffsetInSrcDataFile...currentOffsetInSrcDataFile+buffer.count-1{
//                print(n-currentOffsetInSrcDataFile)
                buffer[n-currentOffsetInSrcDataFile] = allDatas[n]
            }
            
            print("here")
            print(buffer.hexEncodedString().uppercased())
            
            let p1: UInt8 = UInt8((currentFileOffset & 65280) >> 8 )
            let p2: UInt8 = UInt8((currentFileOffset & 255));
            
            updateBinary(tag: tag, session: session, p1: p1, p2: p2, data: buffer) { response in
                
                currentOffsetInSrcDataFile += dataChunkToWrite
                currentFileOffset += dataChunkToWrite
                currentRemainingDataToWrite -= dataChunkToWrite

                self.writeData(tag: tag, session: session, fileOffset: currentFileOffset, image: image, password: password, datas: datas, remainingDataToWrite: currentRemainingDataToWrite)
            }
            
        }else{
            self.selectNdefTagApplication(tag: tag, session: session, sendInterrupt: true)
        }
    }
    
    func verifyPasswordForWriteAccess(tag: NFCISO7816Tag, session: NFCReaderSession, completion: @escaping (Data) -> ()){
        var passwordData: Data = Data(repeating: UInt8(0), count: 16) //Data("123456".utf8) //"ACED0005740006313233343536".hexadecimal!
//        passwordData.append(contentsOf: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])

        let myAPDU = NFCISO7816APDU(instructionClass: 0x00, instructionCode: 0x20, p1Parameter: 0x00, p2Parameter: 0x02, data: passwordData, expectedResponseLength: -1)
        
        session.alertMessage = "Verifying password"
        
        tag.sendCommand(apdu: myAPDU) { (response: Data, sw1: UInt8, sw2: UInt8, error: Error?) in
            guard error == nil && (sw1 == 0x90 && sw2 == 0) else {
                print(error?.localizedDescription ?? "")
                session.invalidate(errorMessage: "Applicationfailure")
                return
            }
            
            completion(response)
        
        }
    }
    
    func disablePasswordVerificationRequirement(tag: NFCISO7816Tag, session: NFCReaderSession, completion: @escaping (Data) -> ()){
        let myAPDU1 = NFCISO7816APDU(instructionClass: 0x00, instructionCode: 0x26, p1Parameter: 0x00, p2Parameter: 0x01, data: Data(), expectedResponseLength: -1)
        let myAPDU2 = NFCISO7816APDU(instructionClass: 0x00, instructionCode: 0x26, p1Parameter: 0x00, p2Parameter: 0x02, data: Data(), expectedResponseLength: -1)

        
        session.alertMessage = "Disabling password verification for the write feature"
        
        tag.sendCommand(apdu: myAPDU1) { (response: Data, sw1: UInt8, sw2: UInt8, error: Error?) in
            guard error == nil && (sw1 == 0x90 && sw2 == 0) else {
                print(error?.localizedDescription ?? "")
                session.invalidate(errorMessage: "Applicationfailure")
                return
            }
            
            tag.sendCommand(apdu: myAPDU2) { response2, sww1, sww2, error2 in
                guard error2 == nil && (sww1 == 0x90 && sww2 == 0) else {
                    print(error?.localizedDescription ?? "")
                    session.invalidate(errorMessage: "Applicationfailure")
                    return
                }
                
                completion(response2)
            }
            
        
        }

    }
    
    func updateBinary(tag: NFCISO7816Tag, session: NFCReaderSession, p1: UInt8, p2: UInt8, data: Data, completion: @escaping (Data) -> ()){
        let myAPDU = NFCISO7816APDU(instructionClass: 0x00, instructionCode: 0xD6, p1Parameter: p1, p2Parameter: p2, data: data, expectedResponseLength: -1)
        
        session.alertMessage = "Updating NDEF file content"
        
        tag.sendCommand(apdu: myAPDU) { (response: Data, sw1: UInt8, sw2: UInt8, error: Error?) in
//            guard error == nil && (sw1 == 0x90 && sw2 == 0) else {
//                print(error?.localizedDescription ?? "")
//                session.invalidate(errorMessage: "Applicationfailure")
//                return
//            }
            
            completion(data)
        
        }
    }
    
    func selectSystemFile(tag: NFCISO7816Tag, session: NFCReaderSession){
        let dataMifare: [UInt8] = [0xE1, 0x01]
        let dataPacketMifare = Data(bytes: dataMifare, count: dataMifare.count)
        let myAPDU = NFCISO7816APDU(instructionClass: 0x00, instructionCode: 0xA4, p1Parameter: 0x00, p2Parameter: 0x0C, data: dataPacketMifare, expectedResponseLength: -1)
        
        session.alertMessage = "Selecting system file"
        
        tag.sendCommand(apdu: myAPDU) { (response: Data, sw1: UInt8, sw2: UInt8, error: Error?) in
            guard error == nil && (sw1 == 0x90 && sw2 == 0) else {
                print(error?.localizedDescription ?? "")
                session.invalidate(errorMessage: "Applicationfailure")
                return
            }

            self.sendGPOStateControlCommand(tag: tag, session: session)

            
        }
    }
    
    func sendGPOStateControlCommand(tag: NFCISO7816Tag, session: NFCReaderSession){
        let data:[UInt8] = [0x00]
        let myAPDUData = Data(data)
        let myAPDU = NFCISO7816APDU(instructionClass: 0xA2, instructionCode: 0xD6, p1Parameter: 0x00, p2Parameter: 0x1F, data: myAPDUData, expectedResponseLength: -1)
        
        session.alertMessage = "Sending GPO state control command"
        
        tag.sendCommand(apdu: myAPDU) { (response: Data, sw1: UInt8, sw2: UInt8, error: Error?) in
            guard error == nil && (sw1 == 0x90 && sw2 == 0) else {
                print(error?.localizedDescription ?? "")
                session.invalidate(errorMessage: "Applicationfailure")
                return
            }

            print("finished")
            session.invalidate()

            
        }
                
    }
        
    func readBinary(tag: NFCISO7816Tag, session: NFCReaderSession, p1: UInt8, p2: UInt8, length: UInt8, completion: @escaping (Data) -> ()){
        
        let myAPDU = NFCISO7816APDU(instructionClass: 0x00, instructionCode: 0xB0, p1Parameter: p1, p2Parameter: p2, data: Data(), expectedResponseLength: Int(length))
        
        session.alertMessage = "Reading NDEF file content"
        
        
        tag.sendCommand(apdu: myAPDU) { (response: Data, sw1: UInt8, sw2: UInt8, error: Error?) in

            completion(response)
        }
    }

}

extension String {
    
    /// Create `Data` from hexadecimal string representation
    ///
    /// This creates a `Data` object from hex string. Note, if the string has any spaces or non-hex characters (e.g. starts with '<' and with a '>'), those are ignored and only hex characters are processed.
    ///
    /// - returns: Data represented by this hexadecimal string.
    
    var hexadecimal: Data? {
        var data = Data(capacity: count / 2)
        
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, range: NSRange(startIndex..., in: self)) { match, _, _ in
            let byteString = (self as NSString).substring(with: match!.range)
            let num = UInt8(byteString, radix: 16)!
            data.append(num)
        }
        
        guard data.count > 0 else { return nil }
        
        return data
    }
    
}

extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
    }

    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return self.map { String(format: format, $0) }.joined()
    }
}
