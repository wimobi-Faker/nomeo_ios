//
//  CCFile.swift
//  nfcTag
//
//  Created by Feker Hassine on 2021-11-03.
//

import Foundation;
class CCFile
{
    var length: Int = 0
    var mappingVersion: Int = 0
    var maxBytesRead: Int = 0
    var maxBytesWrite: Int = 0
    var tField: Int = 0
    var lField: Int = 0
    var fileId: Int = 0
    var maxNdefFileSize: Int = 0
    var readAccess: RightAccess?
    var writeAccess: RightAccess?
    
    static func parse(_ buffer: [UInt32])->CCFile?
    {
        let byteBuffer: ByteBuffer? = ByteBuffer(array: buffer);
        let ccFile: CCFile? = CCFile();
        ccFile!.length = BytesUtils.byteToUnsignedInt(byteBuffer!.get()) << 8;
        ccFile!.length += BytesUtils.byteToUnsignedInt(byteBuffer!.get());
        ccFile!.mappingVersion = BytesUtils.byteToUnsignedInt(byteBuffer!.get());
        ccFile!.maxBytesRead = BytesUtils.byteToUnsignedInt(byteBuffer!.get()) << 8;
        ccFile!.maxBytesRead += BytesUtils.byteToUnsignedInt(byteBuffer!.get());
        ccFile!.maxBytesWrite = BytesUtils.byteToUnsignedInt(byteBuffer!.get()) << 8;
        ccFile!.maxBytesWrite += BytesUtils.byteToUnsignedInt(byteBuffer!.get());
        ccFile!.tField = BytesUtils.byteToUnsignedInt(byteBuffer!.get());
        ccFile!.lField = BytesUtils.byteToUnsignedInt(byteBuffer!.get());
        ccFile!.fileId = BytesUtils.byteToUnsignedInt(byteBuffer!.get()) << 8;
        ccFile!.fileId += BytesUtils.byteToUnsignedInt(byteBuffer!.get());
        ccFile!.maxNdefFileSize = BytesUtils.byteToUnsignedInt(byteBuffer!.get()) << 8;
        ccFile!.maxNdefFileSize += BytesUtils.byteToUnsignedInt(byteBuffer!.get());
        let readAccess: UInt32 = byteBuffer!.get();
        ccFile!.readAccess = readAccess == CommandsUtils.VALUE_UNLOCK ? RightAccess.UNLOCKED : readAccess == CommandsUtils.VALUE_LOCK ? RightAccess.LOCKED : RightAccess.BLOCKED
        let writeAccess: UInt32 = byteBuffer!.get();
        ccFile!.writeAccess = writeAccess == CommandsUtils.VALUE_UNLOCK ? RightAccess.UNLOCKED : writeAccess == CommandsUtils.VALUE_LOCK ? RightAccess.LOCKED : RightAccess.BLOCKED
        return ccFile;
    }
    enum RightAccess
    {
        case UNLOCKED
        case LOCKED
        case BLOCKED
    }
}
class BytesUtils
{
    private static
    var HEX_ARRAY: [Character] = Array();

    class func byteToUnsignedInt(_ byteToConvert: UInt32)->Int
    {
        return Int(byteToConvert & 255)
    }
}
class CommandsUtils
{
    static var VALUE_UNLOCK: UInt32 = UInt32(0)
    static var VALUE_LOCK: UInt32 = UInt32(128);
    static var VALUE_PERMANENTLY_LOCK: UInt32 = UInt32(255);
}

public class ByteBuffer {

    public init(size: Int) {
        array.reserveCapacity(size)
    }
    
    public init(array: [UInt32]) {
        self.array = array
    }

    public func allocate(_ size: Int) {
        array = [UInt32]()
        array.reserveCapacity(size)
        currentIndex = 0
    }

    public func get() -> UInt32 {
        let result = array[currentIndex]
        currentIndex += 1
        return result
    }


    public enum Endianness {
        case little
        case big
    }

    private func to<T>(_ value: T) -> [UInt8] {
        var value = value
        return withUnsafeBytes(of: &value, Array.init)
    }

    private func from<T>(_ value: [UInt8], _: T.Type) -> T {
        return value.withUnsafeBytes {
            $0.load(fromByteOffset: 0, as: T.self)
        }
    }

    private var array = [UInt32]()
    private var currentIndex: Int = 0

    private var currentEndianness: Endianness = .big
    private let hostEndianness: Endianness = OSHostByteOrder() == OSLittleEndian ? .little : .big
}
