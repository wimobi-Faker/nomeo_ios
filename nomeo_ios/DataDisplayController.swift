//
//  DataDisplayController.swift
//  nfcTag
//
//  Created by Feker Hassine on 2021-12-10.
//

import Foundation
import UIKit


class DataDisplayController: UIViewController{
    @IBOutlet weak var field1Label: UILabel!
    @IBOutlet weak var field2Label: UILabel!
    @IBOutlet weak var field3Label: UILabel!
    @IBOutlet weak var field4Label: UILabel!
    @IBOutlet weak var field5Label: UILabel!
    @IBOutlet weak var templateIdLabel: UILabel!
    
    var room: Room?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupData()
    }
    
    func setupData(){
        guard let room = room else {
            return
        }

        field1Label.text = (room.Field1?.isEmpty ?? true) ? "NA" :  room.Field1
        field2Label.text = (room.Field2?.isEmpty ?? true) ? "NA" :  room.Field2
        field3Label.text = (room.Field3?.isEmpty ?? true) ? "NA" :  room.Field3
        field4Label.text = (room.Field4?.isEmpty ?? true) ? "NA" :  room.Field4
        field5Label.text = (room.Field5?.isEmpty ?? true) ? "NA" :  room.Field5
        
        if let templateId = room.TemplateId.Id{
            templateIdLabel.text = String(templateId)
        }
    }
}

//MARK: - ACTIONS
extension DataDisplayController{
    
    @IBAction func backButtonTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
